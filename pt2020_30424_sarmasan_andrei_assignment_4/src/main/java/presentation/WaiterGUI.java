package presentation;

import business.Restaurant;
import data.RestaurantSerializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

public class WaiterGUI {

    private JFrame frame;

    private Restaurant restaurant;


    private JTable table;
    private final String[] columnNames = { "OrderID", "Table ID", "Products ordered"};
    private JScrollPane scrollPane;
    private JPanel panel;

    private Observer chef;

    public WaiterGUI(final Observer chef) {
        frame = new JFrame();
        restaurant = RestaurantSerializator.deserialize();
        this.chef = chef;
        panel = new JPanel();
        String[][] data = restaurant.getOrdersString();
        table = new JTable(data, columnNames);
        scrollPane = new JScrollPane(table);
        panel.setLayout(new GridLayout(0,1));
        final JButton createOrderButton = new JButton("Create order");
        panel.add(createOrderButton);
        final JButton computePriceButton = new JButton("Compute price for an order");
        panel.add(computePriceButton);
        final JButton generateBillButton = new JButton("Generate bill");
        panel.add(generateBillButton);
        final JButton resetOrdersButton = new JButton("Reset orders");
        panel.add(resetOrdersButton);
        panel.add(scrollPane);
        frame.setContentPane(panel);
        frame.setSize(500, 300);
        frame.setTitle("Waiter");
        frame.setLocationRelativeTo(null);
        createOrderButton.addActionListener(e -> new createOrderFrame());
        computePriceButton.addActionListener(e -> new computePriceFrame());
        generateBillButton.addActionListener(e -> new generateBillFrame());
        resetOrdersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to reset orders?","Warning", JOptionPane.YES_NO_OPTION);
                if(dialogResult == JOptionPane.YES_OPTION){
                    restaurant.deleteAllOrders();
                    restaurant.resetNumberOrders();
                    RestaurantSerializator.serialize(restaurant);
                    updateTable();
                }
            }
        });
    }

    private class createOrderFrame extends JFrame {

        private createOrderFrame() {

            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(0, 1));
            final JLabel tableIDLabel = new JLabel("Table ID:");
            panel.add(tableIDLabel);
            final JTextField tableIDTextField = new JTextField();
            panel.add(tableIDTextField);
            final JLabel menuItemsLabel = new JLabel("Menu items");
            panel.add(menuItemsLabel);
            final JTextField menuItemsTextField = new JTextField();
            panel.add(menuItemsTextField);
            final JButton viewMenuItemsButton = new JButton("View menu items");
            panel.add(viewMenuItemsButton);
            final JButton createButton = new JButton("Create Order");
            panel.add(createButton);

            this.setContentPane(panel);
            this.setSize(500,300);
            this.setTitle("Create order");
            this.setLocationRelativeTo(null);
            this.setVisible(true);

            viewMenuItemsButton.addActionListener(e -> {
                restaurant = RestaurantSerializator.deserialize();
                new AdministratorGUI.MenuItemsTable(restaurant);
            });
            createButton.addActionListener(e -> {
                restaurant = RestaurantSerializator.deserialize();
                restaurant.createOrder(chef,  Integer.parseInt(tableIDTextField.getText()), new ArrayList<>(Arrays.asList(menuItemsTextField.getText().split(","))));
                RestaurantSerializator.serialize(restaurant);
                updateTable();
            });
        }
    }

    private void updateTable() {
        panel.remove(scrollPane);
        String[][] data = restaurant.getOrdersString();
        table = new JTable(data, columnNames);
        scrollPane = new JScrollPane(table);
        panel.add(scrollPane);
        frame.setContentPane(panel);
    }

    private class computePriceFrame extends JFrame {

        private JTextField orderIDTextField;

        private computePriceFrame() {
            final JLabel orderID1 = new JLabel("Order ID:");
            orderIDTextField = new JTextField();
            final JButton computeButton = new JButton("Compute price");
            final JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(0, 1));
            panel.add(orderID1);
            panel.add(orderIDTextField);
            panel.add(computeButton);

            this.setContentPane(panel);
            this.setSize(300,150);
            this.setTitle("Compute price for an order");
            this.setLocationRelativeTo(null);
            this.setVisible(true);

            computeButton.addActionListener(e -> {
                int orderID = Integer.parseInt(orderIDTextField.getText());
                double price;
                if((orderID != -1) && ((price = restaurant.computePrice(orderID)) != -1)) {
                    JOptionPane.showMessageDialog(frame, "The order total is $" + price);
                }
            });
        }
    }

    private class generateBillFrame extends JFrame {
        private JTextField tableIDTextField;

        private generateBillFrame() {

            final JPanel panel = new JPanel();
            final JLabel tableID = new JLabel("Table ID:");
            tableIDTextField = new JTextField();
            final JButton generateButton = new JButton("Generate bill");
            panel.setLayout(new GridLayout(0, 1));
            panel.add(tableID);
            panel.add(tableIDTextField);
            panel.add(generateButton);
            setContentPane(panel);
            setSize(300,150);
            setTitle("Generate bill");
            setLocationRelativeTo(null);
            setVisible(true);
            generateButton.addActionListener(e -> {
                int tableID1 = Integer.parseInt(tableIDTextField.getText());
                if(restaurant.generateBill(tableID1)) {
                    updateTable();
                }
            });
        }
    }

    public JFrame getFrame() {
        return frame;
    }
}