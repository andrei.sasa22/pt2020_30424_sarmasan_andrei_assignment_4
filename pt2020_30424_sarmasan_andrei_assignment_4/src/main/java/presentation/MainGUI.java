package presentation;

import javax.swing.*;
import java.awt.*;

public class MainGUI extends JFrame {

    public MainGUI() {

        final AdministratorGUI administratorGUI = new AdministratorGUI();
        final ChefGUI chefGUI = new ChefGUI();
        final WaiterGUI waiterGUI = new WaiterGUI(chefGUI);

        final JButton administratorButton = new JButton("Administrator");
        final JButton waiterButton = new JButton("Waiter");
        final JButton chefButton = new JButton("Chef");

        final JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 1));
        panel.add(administratorButton);
        panel.add(waiterButton);
        panel.add(chefButton);

        setContentPane(panel);
        setTitle("Restaurant Management System");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);

        administratorButton.addActionListener(e -> administratorGUI.setVisible(true));
        waiterButton.addActionListener(e -> waiterGUI.getFrame().setVisible(true));
        chefButton.addActionListener(e -> chefGUI.setVisible(true));
    }
}
