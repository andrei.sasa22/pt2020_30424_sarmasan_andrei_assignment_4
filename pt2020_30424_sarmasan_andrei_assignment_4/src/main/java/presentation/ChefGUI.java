package presentation;

import business.Restaurant;
import data.RestaurantSerializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChefGUI extends JFrame implements Observer {

    private Restaurant restaurant = RestaurantSerializator.deserialize();

    private ArrayList<String> compositeProducts = restaurant.getMenuNames();
    private ArrayList<String> dates = restaurant.getMenuDates();

    private JPanel panel = new JPanel();
    private JTable table;
    private JScrollPane scrollPanel;

    public ChefGUI() {
        final JButton clearAllButton;

        panel.setLayout(new GridLayout(0,1));

        clearAllButton = new JButton("Clear all");
        panel.add(clearAllButton);

        final String[][] data = new String[compositeProducts.size()][2];
        for(int i = 0; i < compositeProducts.size(); i++) {
            data[i][0] = compositeProducts.get(i);
            data[i][1] = dates.get((i));
        }
        table = new JTable(data, new String[]{"Composite Product", "Time added"});
        scrollPanel = new JScrollPane(table);
        panel.add(scrollPanel);

        this.setContentPane(panel);
        this.setSize(500, 300);
        this.setTitle("Chef");
        this.setLocationRelativeTo(null);

        clearAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Restaurant restaurant = RestaurantSerializator.deserialize();
                restaurant.clear();
                RestaurantSerializator.serialize(restaurant);
                panel.remove(scrollPanel);
                String[][] data1 = new String[0][2];
                table = new JTable(data1, new String[]{"Composite Product", "Time added"});
                scrollPanel = new JScrollPane(table);
                panel.add(scrollPanel);
                setContentPane(panel);
            }
        });
    }

    @Override
    public void update(final ArrayList<String> compositeProducts, final ArrayList<String> dates) {
        this.compositeProducts = compositeProducts;
        this.dates = dates;
        panel.remove(scrollPanel);
        final String[][] data = new String[compositeProducts.size()][2];
        for(int i = 0; i < compositeProducts.size(); i++) {
            data[i][0] = compositeProducts.get(i);
            data[i][1] = dates.get((i));
        }
        table = new JTable(data, new String[]{"Composite Product", "Time added"});
        scrollPanel = new JScrollPane(table);
        panel.add(scrollPanel);
        this.setContentPane(panel);
    }
}
