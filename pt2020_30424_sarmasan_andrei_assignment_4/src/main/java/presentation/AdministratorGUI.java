package presentation;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;
import business.Restaurant;
import data.RestaurantSerializator;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdministratorGUI extends JFrame {

    public AdministratorGUI() {

        final Restaurant restaurant = RestaurantSerializator.deserialize();

        final JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 1));
        final JLabel actionLabel = new JLabel("Action");
        panel.add(actionLabel);
        final JComboBox<String> actionComboBox = new JComboBox<>(new String[]{"Create item", "Delete item", "Edit item"});
        panel.add(actionComboBox);
        final JLabel nameLabel = new JLabel("Product name");
        panel.add(nameLabel);
        final JTextField nameTextField = new JTextField();
        panel.add(nameTextField);
        final JLabel priceLabel = new JLabel("Price");
        panel.add(priceLabel);
        final JTextField priceTextField = new JTextField();
        panel.add(priceTextField);
        final JLabel baseProductsLabel = new JLabel("Components - Separated by ,");
        panel.add(baseProductsLabel);
        final JTextField otherProductsTextField = new JTextField();
        panel.add(otherProductsTextField);
        final JButton applyButton = new JButton("Apply");
        panel.add(applyButton);
        final JButton viewItemsButton = new JButton("View all items");
        panel.add(viewItemsButton);
        this.setContentPane(panel);
        this.setSize(600, 400);
        this.setTitle("Administrator");
        this.setLocationRelativeTo(null);

        applyButton.addActionListener(e -> {
            MenuItem menuItem;
            switch (Objects.requireNonNull(actionComboBox.getSelectedItem()).toString()) {
                case "Create item":
                    menuItem = getMenuItem(restaurant, nameTextField, priceTextField, otherProductsTextField);
                    if (menuItem != null) {
                        restaurant.createMenuItem(menuItem);
                    }
                    break;
                case "Delete item":
                    restaurant.deleteMenuItem(nameTextField.getText());
                case "Edit item":
                    menuItem = getMenuItem(restaurant, nameTextField, priceTextField, otherProductsTextField);
                    if (menuItem != null) {
                        restaurant.editMenuItem(menuItem);
                    }
            }
            RestaurantSerializator.serialize(restaurant);
        });
        viewItemsButton.addActionListener(e -> new MenuItemsTable(restaurant));
    }

    private MenuItem getMenuItem(final Restaurant restaurant, final JTextField nameTextField, final JTextField priceTextField, final JTextField otherProductsTextField) {
        final MenuItem menuItem;
        final String[] otherProductsNames;
        if (otherProductsTextField.getText().isEmpty()) {
            otherProductsNames = null;
        }
        else {
            otherProductsNames = otherProductsTextField.getText().split(",");
        }
        if (otherProductsNames == null) {
            menuItem = new BaseProduct();
            menuItem.setName(nameTextField.getText());
            menuItem.setPrice(Double.parseDouble(priceTextField.getText()));
        } else {
            List<MenuItem> menuItems = new ArrayList<>();
            for (String otherProductName : otherProductsNames) {
                MenuItem existingMenuItem = restaurant.getMenuItem(otherProductName);
                if (existingMenuItem == null) {
                    return null;
                }
                menuItems.add(existingMenuItem);
            }
            menuItem = new CompositeProduct();
            menuItem.setName(nameTextField.getText());
            ((CompositeProduct) menuItem).setMenuItems(menuItems);
            menuItem.setPrice(menuItem.getPrice());
        }
        return menuItem;
    }

    static class MenuItemsTable extends JFrame {

        MenuItemsTable(Restaurant restaurant) {
            String[] columnNames = {"Name", "Type", "Components", "Price"};
            String[][] data = restaurant.getMenuString();
            JTable table = new JTable(data, columnNames);
            JScrollPane sp = new JScrollPane(table);
            this.add(sp);
            this.setSize(500, 200);
            this.setLocationRelativeTo(null);
            this.setTitle("Menu items");
            this.setVisible(true);
        }
    }
}
