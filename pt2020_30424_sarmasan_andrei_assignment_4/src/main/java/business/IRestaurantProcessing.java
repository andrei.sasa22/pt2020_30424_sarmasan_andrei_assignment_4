package business;

import presentation.Observer;

import java.util.Collection;

public interface IRestaurantProcessing {

    boolean createMenuItem(final MenuItem menuItem);

    boolean deleteMenuItem(final String name);

    boolean editMenuItem(final MenuItem menuItem);

    boolean createOrder(final Observer observer, final Integer tableID, final Collection<String> menuItemNames);

    double computePrice(final Integer orderID);

    boolean generateBill(final Integer tableID);
}
