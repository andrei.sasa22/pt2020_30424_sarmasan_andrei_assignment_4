package business;

import data.BillWriter;
import presentation.Observer;

import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing {

    private Map<Order, Collection<MenuItem>> orderCollectionHashMap = new HashMap<>();

    private Map<String, MenuItem> menuItemHashMap = new HashMap<>();

    private int orderNr = 0;

    public boolean createMenuItem(final MenuItem menuItem) {
        checkNull(menuItem);
        if (menuItemHashMap.containsKey(menuItem.getName())) {
            return false;
        }
        menuItemHashMap.put(menuItem.getName(), menuItem);
        return true;
    }

    public boolean deleteMenuItem(final String name) {
        checkNull(name);
        if (!menuItemHashMap.containsKey(name)) {
            return false;
        }
        menuItemHashMap.remove(name);
        return true;
    }

    public boolean editMenuItem(final MenuItem menuItem) {
        checkNull(menuItem);
        if (!menuItemHashMap.containsKey(menuItem.getName())) {
            return false;
        }
        menuItemHashMap.put(menuItem.getName(), menuItem);
        return true;
    }

    public boolean createOrder(final Observer chef, final Integer tableID, final Collection<String> menuNames) {
        checkNull(chef);
        checkID(tableID);
        checkNull(menuNames);
        orderNr++;
        final ArrayList<MenuItem> menuItems = new ArrayList<>();
        for (final String menuName : menuNames) {
            if (menuItemHashMap.containsKey(menuName)) {
                menuItems.add(menuItemHashMap.get(menuName));
                if (menuItemHashMap.get(menuName).getClass().getSimpleName().equals("CompositeProduct")) {
                    notifyObservers(chef, menuItemHashMap.get(menuName));
                }
            } else {
                return false;
            }
        }


        final Order order = new Order();
        order.setId(orderNr);
        order.setTableID(tableID);
        orderCollectionHashMap.put(order, menuItems);
        return true;
    }

    public double computePrice(final Integer id) {
        checkID(id);
        double price;
        final Order order = new Order();
        order.setId(id);
        if (!orderCollectionHashMap.containsKey(order)) {
            price = -1;
        } else {
            price = orderCollectionHashMap.get(order).stream().mapToDouble(MenuItem::getPrice).sum();
        }
        return price;
    }

    public boolean generateBill(final Integer id) {
        checkID(id);
        boolean foundID = false;
        final Map<Integer, Map<String, Double>> orders = new HashMap<>();
        for (final Order order : orderCollectionHashMap.keySet()) {
            if (order.getTableID().equals(id)) {
                foundID = true;
                final Map<String, Double> products = new HashMap<>();
                for (final MenuItem menuItem : orderCollectionHashMap.get(order)) {
                    products.put(menuItem.getName(), menuItem.computePrice());
                }
                orders.put(order.getId(), products);
                orderCollectionHashMap.remove(order);
            }
        }
        if (foundID) {
            BillWriter.printBill(orders);
        }
        return foundID;
    }

    public void deleteAllOrders() {
        orderCollectionHashMap.clear();
    }

    private static void checkID(final Integer integer) {
        assert integer >= 1;
    }

    private static void checkNull(final Object object) {
        assert object != null;
    }

    public MenuItem getMenuItem(final String menuItemName) {
        return menuItemHashMap.get(menuItemName);
    }

    public void resetNumberOrders() {
        orderNr = 0;
    }

    public String[][] getMenuString() {
        final String[][] menu = new String[menuItemHashMap.size()][4];
        int index = 0;
        for (final MenuItem menuItem : menuItemHashMap.values()) {
            menu[index][0] = menuItem.getName();
            menu[index][1] = menuItem.getClass().getSimpleName();
            menu[index][2] = "-";
            if(menu[index][1].equals("CompositeProduct")) {
                final StringBuilder stringBuilder = new StringBuilder();
                for (MenuItem compositeProductItem : ((CompositeProduct) menuItem).getMenuItems()) {
                    stringBuilder.append(compositeProductItem.getName());
                    stringBuilder.append(",");
                }
                stringBuilder.deleteCharAt(stringBuilder.toString().length() - 1);
                menu[index][2] = stringBuilder.toString();
            }
            menu[index][3] = "$" + menuItem.computePrice();
            index++;
        }
        return menu;
    }

    public String[][] getOrdersString() {
        final String[][] orders = new String[orderCollectionHashMap.size()][3];
        int index = 0;
        for (final Order order : orderCollectionHashMap.keySet()) {
            orders[index][0] = String.valueOf(order.getId());
            orders[index][1] = String.valueOf(order.getTableID());
            final StringBuilder stringBuilder = new StringBuilder();
            for (final MenuItem menuItem : orderCollectionHashMap.get(order)) {
                stringBuilder.append(menuItem.getName());
                stringBuilder.append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.toString().length() - 1);
            orders[index][2] = stringBuilder.toString();
            index++;
        }
        return orders;
    }
}
