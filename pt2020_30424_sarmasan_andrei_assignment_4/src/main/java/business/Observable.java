package business;

import presentation.Observer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Observable implements Serializable {

    private ArrayList<String> menuNames = new ArrayList<>();

    private ArrayList<String> menuDates = new ArrayList<>();

    public void notifyObservers(final Observer observer, final MenuItem menuItem) {
        menuNames.add(menuItem.getName());
        final String date = DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalDateTime.now());
        for(final String string : menuNames) {
            menuDates.add(date);
        }
        observer.update(menuNames, menuDates);
    }

    public void clear() {
        menuNames.clear();
        menuDates.clear();
    }

    public ArrayList<String> getMenuNames() {
        return menuNames;
    }

    public void setMenuNames(ArrayList<String> menuNames) {
        this.menuNames = menuNames;
    }

    public ArrayList<String> getMenuDates() {
        return menuDates;
    }

    public void setMenuDates(ArrayList<String> menuDates) {
        this.menuDates = menuDates;
    }
}
