package business;

import java.util.List;

public class CompositeProduct extends MenuItem {

    private List<MenuItem> menuItems;

    @Override
    public double computePrice() {
        return menuItems.stream().mapToDouble(MenuItem::getPrice).sum();
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
