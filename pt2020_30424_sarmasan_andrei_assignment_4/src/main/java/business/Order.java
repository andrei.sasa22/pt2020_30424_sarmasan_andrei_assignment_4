package business;

import java.io.Serializable;
import java.util.Objects;

public class Order implements Serializable {

    private Integer id;

    private Integer tableID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTableID() {
        return tableID;
    }

    public void setTableID(Integer tableID) {
        this.tableID = tableID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(tableID, order.tableID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tableID);
    }
}
