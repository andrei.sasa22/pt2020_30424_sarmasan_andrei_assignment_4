package business;

public class BaseProduct extends MenuItem {
    @Override
    public double computePrice() {
        return getPrice();
    }
}
