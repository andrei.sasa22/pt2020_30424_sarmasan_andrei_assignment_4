package data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class BillWriter {
    public static void printBill(final Map<Integer, Map<String, Double>> orders) {
        final String date = DateTimeFormatter.ofPattern("yyyy:MM:dd: HH:mm:ss").format(LocalDateTime.now()).replaceAll(":", "").replaceAll(" ", "");
        final String bill = "bill" + date + ".txt";
        try {
            final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(bill));
            double total = 0;
            for(int order : orders.keySet()) {
                bufferedWriter.write("Order " + order + ":\n");
                for(String product : orders.get(order).keySet()) {
                    bufferedWriter.write("\t" + product + " - $" + orders.get(order).get(product) + "\n");
                    total += orders.get(order).get(product);
                }
            }
            bufferedWriter.write("\nTOTAL: $" + total + "\nDATE: " + date);
            bufferedWriter.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
