package data;

import business.Restaurant;

import java.io.*;

public class RestaurantSerializator {

    public static Restaurant deserialize() {
        Restaurant restaurant = null;
        try {
            final FileInputStream fileInputStream = new FileInputStream("Restaurant.ser");
            final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            restaurant = (Restaurant) objectInputStream.readObject();
            fileInputStream.close();
            objectInputStream.close();
            return restaurant;
        } catch (IOException i) {
            System.out.println("Could not fin Restaurant.ser. Creating a new one");
            restaurant = new Restaurant();
            serialize(restaurant);
            return restaurant;
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
            return new Restaurant();
        }
    }

    public static void serialize(final Restaurant restaurant) {
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream("Restaurant.ser");
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(restaurant);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}